import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ChangePassword } from "./../../models/ChangePassword.class";

@Injectable({
	providedIn: 'root'
})
export class AdminService {


	public API: string = 'http://localhost:8080/rest/admin';
	public httpClient: HttpClient;

	constructor(httpClient: HttpClient) {
		this.httpClient = httpClient;
	}

	changePasword(ChangePassword: ChangePassword): Observable<any> {
		return this.httpClient.post(`${this.API}/changePassword`, ChangePassword);
	}

	handleError(err: any) {
		if (err.error instanceof Error) {
			console.log('client site error : ' + err.message);
		}
		else {
			console.log('client site erro ' + err.status + ' - ' + err.message);
		}
	}
}
