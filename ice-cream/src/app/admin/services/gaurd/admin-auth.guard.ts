import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as JWT from 'jwt-decode';

@Injectable({
	providedIn: 'root'
})
export class AdminAuthGuard implements CanActivate {

	constructor(private router: Router) { }

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		let currentUser = JSON.parse(localStorage.getItem('currentUser') == null ? sessionStorage.getItem('currentUser') : localStorage.getItem('currentUser'));
		if (currentUser && currentUser.token) {

			if (JWT(currentUser.token)['scopes'] && JWT(currentUser.token)['scopes'].authority && JWT(currentUser.token)['scopes'].authority === "ROLE_ADMIN") {

				return true;
			}
		}
		this.router.navigate(['login']);
		return false;
	}
}
