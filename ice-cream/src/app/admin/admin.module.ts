import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedModule } from "./../shared/shared.module";


import { HomeComponent } from './compoments/home/home.component';
import { HeaderComponent } from './compoments/header/header.component';
import { CustomerManagementComponent } from './compoments/customer-management/customer-management.component';
import { CustomerDetailComponent } from './compoments/customer-detail/customer-detail.component';
import { RecipesManagementComponent } from './compoments/recipes-management/recipes-management.component';
import { AddNewRecipeComponent } from './compoments/add-new-recipe/add-new-recipe.component';
import { UserRecipeManagementComponent } from './compoments/user-recipe-management/user-recipe-management.component';
import { OnlineOrderManagementComponent } from './compoments/online-order-management/online-order-management.component';
import { OrderDetailComponent } from './compoments/order-detail/order-detail.component';
import { FeedbackComponent } from './compoments/feedback/feedback.component';
import { FaqComponent } from './compoments/faq/faq.component';
import { PrizeManagementComponent } from './compoments/prize-management/prize-management.component';
import { AdminAuthGuard } from "./services/gaurd/admin-auth.guard";
import { LoginComponent } from './compoments/login/login.component';
import { AdminChangePasswordComponent } from './compoments/admin-change-password/admin-change-password.component';
import { RecipeReviewComponent } from './compoments/recipe-review/recipe-review.component';
import { EditRecipeComponent } from './compoments/edit-recipe/edit-recipe.component';
import { FeedbackAndFAQManagementComponent } from './compoments/feedback-and-faqmanagement/feedback-and-faqmanagement.component';
import { AddNewFaqComponent } from './compoments/add-new-faq/add-new-faq.component';
import { UserRecipeDetailComponent } from './compoments/user-recipe-detail/user-recipe-detail.component';

const appRoutes: Routes = [
	{
		path: 'admin',
		children: [{
			path: "",
			pathMatch: 'full',
			/*canActivate: [AdminAuthGuard],*/
			component: HomeComponent
		},
		{
			path: "customers",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: CustomerManagementComponent

		},
		{
			path: "customers/:id",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: CustomerDetailComponent

		},
		{
			path: "recipes",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: RecipesManagementComponent

		},
		{
			path: "recipes/:id",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: EditRecipeComponent

		},
		{
			path: "recipes/recipe/add",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: AddNewRecipeComponent

		},
		{
			path: "recipes/review/:id",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: RecipeReviewComponent

		},

		{
			path: "orders",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: OnlineOrderManagementComponent

		},
		{
			path: "orders/:id",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: OrderDetailComponent

		},
		{
			path: "feedbackFAQ",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: FeedbackAndFAQManagementComponent

		},
		{
			path: "faq",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: FaqComponent

		},
		{
			path: "faq/add",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: AddNewFaqComponent

		},
		{
			path: "prize",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: PrizeManagementComponent

		},
		{
			path: "user/recipes",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: UserRecipeManagementComponent

		},
		{
			path: "user/recipes/:id",
			pathMatch: 'full',
			canActivate: [AdminAuthGuard],
			component: UserRecipeDetailComponent

		}
		]
	}
];
@NgModule({
	imports: [
		CommonModule,
		BrowserModule,
		ReactiveFormsModule,
		FormsModule,
		SharedModule,
		RouterModule.forChild(appRoutes),
		HttpClientModule
	],
	declarations: [HomeComponent, HeaderComponent, CustomerManagementComponent,
		CustomerDetailComponent, RecipesManagementComponent, AddNewRecipeComponent, UserRecipeManagementComponent, OnlineOrderManagementComponent, OrderDetailComponent, FeedbackComponent, FaqComponent, PrizeManagementComponent, LoginComponent, AdminChangePasswordComponent, RecipeReviewComponent, EditRecipeComponent, FeedbackAndFAQManagementComponent, AddNewFaqComponent, UserRecipeDetailComponent]
})
export class AdminModule { }
