import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { YourRecipeService } from "./../../../services/your-recipe.service";
import { UserRecipe } from "./../../../models/UserReipe.class";

@Component({
	selector: 'app-prize-management',
	templateUrl: './prize-management.component.html',
	styleUrls: ['./prize-management.component.css']
})
export class PrizeManagementComponent implements OnInit, OnDestroy {

	private subscription: Subscription;
	public userRecipe: UserRecipe[];

	constructor(private router: Router, private yourRecipeService: YourRecipeService) { }

	ngOnInit() {
		this.subscription = this.yourRecipeService.getAllUserRecipes().subscribe(res => {
			this.userRecipe = res;
		});
  }


	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

	}

}
