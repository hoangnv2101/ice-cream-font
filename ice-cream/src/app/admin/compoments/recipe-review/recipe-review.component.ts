import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Ricipe } from "./../../../models/Recipe.class";
import { RecipeService } from "./../../../services/recipe.service";
@Component({
	selector: 'app-recipe-review',
	templateUrl: './recipe-review.component.html',
	styleUrls: ['./recipe-review.component.css']
})
export class RecipeReviewComponent implements OnInit, OnDestroy {

	public ricipe: Ricipe;
	public subscription: Subscription;
	constructor(private router: Router, private activatedRoute: ActivatedRoute, private recipeService: RecipeService) { }

	ngOnInit() {
		this.loadData();
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	loadData() {
		this.subscription = this.activatedRoute.params.subscribe(data => {
			let id = +data.id;
			if (id) {
				this.recipeService.getRecipeById(id).subscribe(res => {
					this.ricipe = res;
				}, error => {
					this.recipeService.handleError(error);
				});
			}

		});

	}

	backToList()
	{
		this.router.navigate(['admin/recipes']);
	}

	backToEdit(id:number)
	{
		this.router.navigate([`admin/recipes/${id}`]);
	}

}
