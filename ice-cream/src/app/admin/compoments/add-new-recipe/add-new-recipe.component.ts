import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Ricipe } from "./../../../models/Recipe.class";
import { RecipeService } from "./../../../services/recipe.service";

@Component({
	selector: 'app-add-new-recipe',
	templateUrl: './add-new-recipe.component.html',
	styleUrls: ['./add-new-recipe.component.css']
})
export class AddNewRecipeComponent implements OnInit, OnDestroy {

	public recipeForm: FormGroup;
	public ricipe: Ricipe;
	public subscription: Subscription;
	public file: File;

	constructor(private activatedRoute: ActivatedRoute, private recipeService: RecipeService,
		private router: Router) {
		this.ricipe = new Ricipe();
	}

	ngOnInit() {

		this.recipeForm = new FormGroup({
			'name': new FormControl(this.ricipe.name, [
				Validators.required
			]),
			'image': new FormControl(this.ricipe.image, [
				Validators.required
			]),
			'description': new FormControl(this.ricipe.description, [
				Validators.required
			]),
			'details': new FormControl(this.ricipe.details, [
				Validators.required
			]),
			'author': new FormControl(this.ricipe.author, [
				Validators.required
			]),
			'enablestatus': new FormControl(this.ricipe.enablestatus, [
			])
		});

	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}


	onBackForm() {
		this.router.navigate(['recipes'], {
			relativeTo: this.activatedRoute.parent
		});
	}
	changeListener($event): void {
		this.file = $event.target.files[0];
		console.log(this.recipeForm);
	}

	onSubmitRecipe() {
		if (!this.recipeForm.invalid) {
			this.ricipe = this.recipeForm.value;
			if (!this.ricipe.enablestatus) {
				this.ricipe.enablestatus = true;
			}
			if (this.file) {
				this.ricipe.image = "images/" + this.file.name;
			}
			this.ricipe.uploadDate = new Date();


			this.subscription = this.recipeService.createRecipe(this.file, this.ricipe).subscribe(res => {
				this.onBackForm();
			}, error => {
				this.recipeService.handleError(error);
			});
		}
	}
}
