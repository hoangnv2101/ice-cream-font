import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { FAQ } from "./../../../models/FAQ.class";
import { FaqService } from "./../../../services/faq.service";


@Component({
	selector: 'app-add-new-faq',
	templateUrl: './add-new-faq.component.html',
	styleUrls: ['./add-new-faq.component.css']
})
export class AddNewFaqComponent implements OnInit, OnDestroy {
	public faqForm: FormGroup;
	public faq: FAQ;
	public subscription: Subscription;

	constructor(private activatedRoute: ActivatedRoute, private faqService: FaqService,
		private router: Router) {
		this.faq = new FAQ();
	}

	ngOnInit() {

		this.faqForm = new FormGroup({
			'question': new FormControl(this.faq.question, [
				Validators.required
			]),
			'answer': new FormControl(this.faq.answer, [
				Validators.required
			])
		});
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	onSubmitFaq() {
		if (!this.faqForm.invalid) {
			this.faq = this.faqForm.value;
			this.subscription = this.faqService.createFaq(this.faq).subscribe(res => {
				this.onBackForm();
			}, error => {
				this.faqService.handleError(error);
			});
		}
	}

	onBackForm() {
		this.router.navigate(['faq'], {
			relativeTo: this.activatedRoute.parent
		});
	}

}
