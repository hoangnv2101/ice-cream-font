import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { UserRecipe } from "./../../../models/UserReipe.class";
import { YourRecipeService } from "./../../../services/your-recipe.service";


@Component({
	selector: 'app-user-recipe-management',
	templateUrl: './user-recipe-management.component.html',
	styleUrls: ['./user-recipe-management.component.css']
})
export class UserRecipeManagementComponent implements OnInit {

	public userRecipes: UserRecipe[];
	public subscription: Subscription;
	public deleteSubscription: Subscription;

	constructor(private yourRecipeService: YourRecipeService, private router: Router) { }

	ngOnInit() {
		this.loadData();
	}

	loadData() {
		this.subscription = this.yourRecipeService.getAllUserRecipes().subscribe(res => {
			this.userRecipes = res;
		})
	}

	onEdit(id: any) {

	}

	onPrize(id: any) {
		this.router.navigate([`admin/user/recipes/${id}`])
	}

	onDelete(id: number) {
		this.deleteSubscription = this.yourRecipeService.delete(+id).subscribe(res =>{
			console.log(res);
			window.location.reload();
		})
	}

}
