import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRecipeManagementComponent } from './user-recipe-management.component';

describe('UserRecipeManagementComponent', () => {
  let component: UserRecipeManagementComponent;
  let fixture: ComponentFixture<UserRecipeManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRecipeManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRecipeManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
