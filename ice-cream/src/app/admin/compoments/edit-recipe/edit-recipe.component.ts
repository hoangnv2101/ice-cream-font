import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Ricipe } from "./../../../models/Recipe.class";
import { RecipeService } from "./../../../services/recipe.service";

@Component({
	selector: 'app-edit-recipe',
	templateUrl: './edit-recipe.component.html',
	styleUrls: ['./edit-recipe.component.css']
})
export class EditRecipeComponent implements OnInit, OnDestroy {

	public recipeForm: FormGroup;
	public ricipe: Ricipe;
	public subscription: Subscription;
	public file: File;

	constructor(private activatedRoute: ActivatedRoute, private recipeService: RecipeService,
		private router: Router) {
		this.ricipe = new Ricipe();
	}

	ngOnInit() {

		this.recipeForm = new FormGroup({
			'id': new FormControl({ value: this.ricipe.id, disabled: true }),
			'name': new FormControl(this.ricipe.name, [
				Validators.required
			]),
			'image': new FormControl(this.ricipe.image, [
			]),
			'description': new FormControl(this.ricipe.description, [
				Validators.required
			]),
			'details': new FormControl(this.ricipe.details, [
				Validators.required
			]),
			'author': new FormControl(this.ricipe.author, [
				Validators.required
			]),
			'viewNumber': new FormControl({ value: this.ricipe.viewNumber, disabled: true }),
			'uploadDate': new FormControl({ value: this.ricipe.uploadDate, disabled: true }),
			'enablestatus': new FormControl({ value: this.ricipe.enablestatus, disabled: true })
		});

		this.loadData();

	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	loadData() {
		this.subscription = this.activatedRoute.params.subscribe(data => {
			let id = +data.id;
			if (id) {
				this.recipeService.getRecipeById(id).subscribe(res => {
					this.ricipe = res;
					this.recipeForm.controls['id'].setValue(this.ricipe.id);
					this.recipeForm.controls['name'].setValue(this.ricipe.name);
					this.recipeForm.controls['description'].setValue(this.ricipe.description);
					this.recipeForm.controls['details'].setValue(this.ricipe.details);
					this.recipeForm.controls['author'].setValue(this.ricipe.author);
					this.recipeForm.controls['viewNumber'].setValue(this.ricipe.viewNumber);
					this.recipeForm.controls['viewNumber'].setValue(this.ricipe.viewNumber);
					this.recipeForm.controls['enablestatus'].setValue(this.ricipe.enablestatus);
					this.recipeForm.controls['uploadDate'].setValue(new Date(this.ricipe.uploadDate).toLocaleDateString());
				}, error => {
					this.recipeService.handleError(error);
				});
			}

		});

	}

	onBackForm() {
		this.router.navigate(['recipes'], {
			relativeTo: this.activatedRoute.parent
		});
	}
	changeListener($event): void {
		this.file = $event.target.files[0];
	}

	onSubmitRecipe() {
		if (!this.recipeForm.invalid) {
			let id = this.ricipe.id;
			this.ricipe = this.recipeForm.value;
			this.ricipe.id = id;

			if (this.file) {
				this.ricipe.image = "images/" + this.file.name;
			}


			this.subscription = this.recipeService.updateRicipe(this.file, this.ricipe).subscribe(res => {
				this.onBackForm();
			}, error => {
				this.recipeService.handleError(error);
			});
		}
	}
}

