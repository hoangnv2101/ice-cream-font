import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { FeedbackService } from "./../../../services/feedback.service";
import { Feedback } from "./../../../models/Feedback.class";


@Component({
	selector: 'app-feedback-and-faqmanagement',
	templateUrl: './feedback-and-faqmanagement.component.html',
	styleUrls: ['./feedback-and-faqmanagement.component.css']
})
export class FeedbackAndFAQManagementComponent implements OnInit, OnDestroy {

	private subscription: Subscription;
	public feedbacks: Feedback[];

	constructor(private router: Router, private feedbackService: FeedbackService) { }

	ngOnInit() {
		this.subscription = this.feedbackService.getAll().subscribe(res => {
			this.feedbacks = res;
		});
	}


	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

	}

	onDelete(id:number)
	{
		this.feedbackService.delete(+id).subscribe(res=>{
			window.location.reload();
		});
	}

}
