import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedbackAndFAQManagementComponent } from './feedback-and-faqmanagement.component';

describe('FeedbackAndFAQManagementComponent', () => {
  let component: FeedbackAndFAQManagementComponent;
  let fixture: ComponentFixture<FeedbackAndFAQManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackAndFAQManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackAndFAQManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
