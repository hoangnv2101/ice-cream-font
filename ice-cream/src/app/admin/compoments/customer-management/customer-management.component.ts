import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CustomerService } from "./../../../services/customer.service";
import { Customer } from "./../../../models/Customer.class";

import * as moment from 'moment';

@Component({
	selector: 'app-customer-management',
	templateUrl: './customer-management.component.html',
	styleUrls: ['./customer-management.component.css']
})
export class CustomerManagementComponent implements OnInit, OnDestroy {
	public subscription: Subscription;
	public updateSubscription: Subscription;
	constructor(private customerService: CustomerService, private router:Router) { }

	customers: Customer[];
	customer: Customer;
	updateCustomers = new Array<Customer>();

	ngOnInit() {
		this.subscription = this.customerService.getCustomers().subscribe(res => {
			this.customers = res;
		});
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
		if (this.updateSubscription) {
			this.updateSubscription.unsubscribe();
		}
	}

	onChecked(e, id: number) {
		let cus = new Customer();
		cus.enableStatus = e.target.checked;
		cus.id = id;
		this.updateCustomers.push(cus);
	}

	updateCustomerStatus() {
		this.updateSubscription = this.customerService.updateListCustomers(this.updateCustomers).subscribe(res => {
			window.location.reload();
		});
	}

	onEdit(id:number)
	{
		this.router.navigate([`admin/customers/${id}`])
	}

}
