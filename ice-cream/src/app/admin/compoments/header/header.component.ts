import { Component, OnInit } from '@angular/core';
import { TokenStorage } from "./../../../utils/TokenStorage.class";

@Component({
	selector: 'admin-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

	constructor(private tokenStorage: TokenStorage) { }

	ngOnInit() {
	}

	logout() {
		this.tokenStorage.signOut();
		window.location.reload();

	}

}
