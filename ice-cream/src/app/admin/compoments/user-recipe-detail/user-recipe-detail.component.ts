import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { YourRecipeService } from "./../../../services/your-recipe.service";
import { UserRecipe } from "./../../../models/UserReipe.class";

@Component({
	selector: 'app-user-recipe-detail',
	templateUrl: './user-recipe-detail.component.html',
	styleUrls: ['./user-recipe-detail.component.css']
})
export class UserRecipeDetailComponent implements OnInit, OnDestroy {
	private subscription: Subscription;
	private updateSubscription: Subscription;
	public userRecipe: UserRecipe;

	constructor(private activatedRoute: ActivatedRoute, private router: Router, private yourRecipeService: YourRecipeService) { }


	ngOnInit() {
		this.subscription = this.activatedRoute.params.subscribe(data => {
			let id = +data.id;
			if (id) {
				this.yourRecipeService.getRecipe(id).subscribe(res => {
					this.userRecipe = res;
				})
			}
		});
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

		if (this.updateSubscription) {
			this.updateSubscription.unsubscribe();
		}

	}


	onUpdateStatus() {
		this.updateSubscription = this.yourRecipeService.updateStatus(this.userRecipe.id, this.userRecipe).subscribe(res => {
			this.router.navigate(['/admin/user/recipes']);
		})
	}

	onBack(){
		this.router.navigate(['/admin/user/recipes']);
	}
}
