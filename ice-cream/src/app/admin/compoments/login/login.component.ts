import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthenticateService } from "./../../../services/authenticate.service";
import { TokenStorage } from "./../../../utils/TokenStorage.class";
import { Login } from "./../../../models/Login.class";
import { LoginUser } from "./../../../models/LoginUser.class";


@Component({
	selector: 'admin-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements  OnInit, OnDestroy {
	public loginForm: FormGroup;
	public login: Login;
	public router: Router;
	public subscription: Subscription;
	public authenticateService: AuthenticateService;
	public isRemember: boolean = false;
	public username: string;
	public tokenStorage: TokenStorage;

	constructor(private activatedRoute: ActivatedRoute, authenticateService: AuthenticateService, router: Router, tokenStorage: TokenStorage) {
		this.login = new Login();
		this.router = router;
		this.authenticateService = authenticateService;
		this.tokenStorage = tokenStorage;
	}

	ngOnInit() {
		this.loginForm = new FormGroup({
			'username': new FormControl(this.login.username, [
				Validators.required,
				Validators.minLength(4),
				Validators.maxLength(100)
			]),
			'password': new FormControl(this.login.password, [
				Validators.required
			])
		});
	}

	onSubmit() {
		if (!this.loginForm.invalid) {
			this.login = this.loginForm.value;

			this.subscription = this.authenticateService.loginUser(this.login).subscribe(res => {
				let usr: LoginUser = this.tokenStorage.parseToken(res.token);
				if (usr.role === "ROLE_ADMIN") {
					this.tokenStorage.saveToken(res, this.isRemember);
					window.location.reload();
				}
			}, error => {
				this.authenticateService.handleError(error);
			});
		}
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

}
