import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { FaqService } from "./../../../services/faq.service";
import { FAQ } from "./../../../models/FAQ.class";


@Component({
	selector: 'app-faq',
	templateUrl: './faq.component.html',
	styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit, OnDestroy {

	private subscription: Subscription;
	public faqs: FAQ[];

	constructor(private router: Router, private faqService: FaqService) { }

	ngOnInit() {
		this.subscription = this.faqService.getAll().subscribe(res => {
			this.faqs = res;
		});
	}


	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

	}

	onDelete(id:number)
	{
		this.faqService.delete(+id).subscribe(res=>{
			window.location.reload();
		});
	}

}
