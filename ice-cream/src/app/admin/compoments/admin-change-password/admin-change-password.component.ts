import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AdminService } from "./../../services/admin.service";
import { ChangePassword } from "./../../../models/ChangePassword.class";
import {TokenStorage} from "./../../../utils/TokenStorage.class";


function passwordConfirming(c: AbstractControl): any {
	if (!c.parent || !c) return;
	const pwd = c.parent.get('password');
	const cpwd = c.parent.get('confirm_password')

	if (!pwd || !cpwd) return;
	if (pwd.value !== cpwd.value) {
		return { invalid: true };

	}
}


@Component({
  selector: 'admin-change-password',
  templateUrl: './admin-change-password.component.html',
  styleUrls: ['./admin-change-password.component.css']
})
export class AdminChangePasswordComponent implements OnInit, OnDestroy {

 	public changePassword: ChangePassword;
	public subscription: Subscription;
	public changePassForm: FormGroup;

	constructor(private adminService: AdminService, private router: Router, 
		private tokenStorage:TokenStorage) {
		this.changePassword = new ChangePassword();
	}

	ngOnInit() {

		this.changePassForm = new FormGroup({
			'oldPass': new FormControl(this.changePassword.oldPass, [
				Validators.required
			]),
			'password': new FormControl(this.changePassword.password, [
				Validators.required,
				Validators.minLength(8),
				Validators.maxLength(20)
			]),
			'confirm_password': new FormControl(null, [
				Validators.required,
				passwordConfirming,
				Validators.minLength(8),
				Validators.maxLength(20)

			])
		});

	}

	onSubmitChangPassword(){
			if (!this.changePassForm.invalid) {
			this.changePassword = this.changePassForm.value;
			this.subscription = this.adminService.changePasword(this.changePassword).subscribe(res => {
			this.tokenStorage.signOut();
			window.location.reload();
			}, error => {
				this.adminService.handleError(error);
			});
		}
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

	}

	onReset()
	{
		this.changePassForm.reset();
	}

}
