import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineOrderManagementComponent } from './online-order-management.component';

describe('OnlineOrderManagementComponent', () => {
  let component: OnlineOrderManagementComponent;
  let fixture: ComponentFixture<OnlineOrderManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineOrderManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineOrderManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
