import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { OrderService } from "./../../../services/order.service";
import { OrderBook } from "./../../../models/OrderBook.class";


@Component({
	selector: 'app-online-order-management',
	templateUrl: './online-order-management.component.html',
	styleUrls: ['./online-order-management.component.css']
})
export class OnlineOrderManagementComponent implements OnInit, OnDestroy {

	constructor(private router: Router, private orderService: OrderService) { }

	private subscription: Subscription;
	public orderBooks: OrderBook[];

	ngOnInit() {
		this.subscription = this.orderService.getAll().subscribe(res => {
			this.orderBooks = res;
		});
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}
}
