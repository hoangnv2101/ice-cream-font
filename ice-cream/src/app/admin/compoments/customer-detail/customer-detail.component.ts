import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Customer } from "./../../../models/Customer.class";
import { CustomerService } from "./../../../services/customer.service";
import * as moment from 'moment';

@Component({
	selector: 'app-customer-detail',
	templateUrl: './customer-detail.component.html',
	styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit, OnDestroy {

	public subscription: Subscription;
	public customer: Customer;
	public expiredDate: Date;

	constructor(private activatedRoute: ActivatedRoute, private customerService: CustomerService) {
		this.customer = new Customer();
	}

	ngOnInit() {

		this.loadData();
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	loadData() {
		this.subscription = this.activatedRoute.params.subscribe(data => {
			let id = +data.id;
			this.customerService.getCustomer(id).subscribe(res => {
				this.customer = res;
			}, error => {
				this.customerService.handleError(error);
			});
		});
	}

	onUpdateUserdetail(expired: any, enable: any) {
		this.expiredDate = new Date(this.customer.expiredDate);
		let day = +expired.value;;
		this.expiredDate.setDate(this.expiredDate.getDate() + day);
		this.customer.expiredDate = this.expiredDate;
		this.customer.enableStatus = enable.value;

		this.customerService.updateCustomerById(this.customer.id, this.customer).subscribe(res => {
			window.location.reload();
		});
	}

}
