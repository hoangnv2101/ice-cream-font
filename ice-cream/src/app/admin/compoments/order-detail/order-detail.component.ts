import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { OrderService } from "./../../../services/order.service";
import { OrderBook } from "./../../../models/OrderBook.class";

@Component({
	selector: 'app-order-detail',
	templateUrl: './order-detail.component.html',
	styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit, OnDestroy {

	private subscription: Subscription;
	private updateSubscription: Subscription;
	public orderBook: OrderBook;

	constructor(private activatedRoute: ActivatedRoute, private router: Router, private orderService: OrderService) { }


	ngOnInit() {
		this.subscription = this.activatedRoute.params.subscribe(data => {
			let id = +data.id;
			if (id) {
				this.orderService.getOrder(id).subscribe(res => {
					this.orderBook = res;
				})
			}
		});
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

		if(this.updateSubscription)
		{
			this.updateSubscription.unsubscribe();
		}

	}

	onUpdateStatus(){
		this.updateSubscription = this.orderService.updateStatus(this.orderBook.id, this.orderBook).subscribe(res => {
			this.router.navigate(['/admin/orders']);
		})
	}

}
