import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { RecipeService } from "./../../../services/recipe.service";
import { Ricipe } from "./../../../models/Recipe.class";



@Component({
	selector: 'app-recipes-management',
	templateUrl: './recipes-management.component.html',
	styleUrls: ['./recipes-management.component.css']
})
export class RecipesManagementComponent implements OnInit, OnDestroy {

	private subscription: Subscription;
	public recipes: Ricipe[];

	constructor(private router: Router, private recipeService: RecipeService) { }

	ngOnInit() {
		this.subscription = this.recipeService.getTAllRecipes().subscribe(res => {
			this.recipes = res;
		});
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

	}

	onEdit(id: any) {
		this.router.navigate([`admin/recipes/${id}`])
	}

	onReview(id: any) {
		this.router.navigate([`admin/recipes/review/${id}`])
	}

}
