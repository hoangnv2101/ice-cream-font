import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { TokenStorage } from "./../../../utils/TokenStorage.class";

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	islogin: boolean = false;

	constructor(private router: Router, private tokenStorage: TokenStorage) { }


	ngOnInit() {
		let currentUser = this.tokenStorage.getLoginUser();
		if (currentUser && currentUser.role === "ROLE_ADMIN") {
			this.islogin = true;
		} else {
			this.islogin = false;
		}
	}

}
