export class OrderBook {
	public id:number;
	public name : string;
	public email: string;
	public contact: string;
	public address:string;
	public bookCost:number;
	public payingOption: string;
	public orderDate: Date;
	public status: boolean;
	public cardNumber: number;
	public expiration:string;
	public cvv:number;
	public cardName:string;

	constructor(){}
}