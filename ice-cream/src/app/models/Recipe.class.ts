export class Ricipe {
	public id : number;
	public name: string;
	public image:string;
	public description: string;
	public details: string;
	public author:string;
	public viewNumber: number;
	public uploadDate:Date;
	public enablestatus: boolean;

	constructor()
	{

	}
}