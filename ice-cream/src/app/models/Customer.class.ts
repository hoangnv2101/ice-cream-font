export class Customer {
	public id:number;
	public username : string;
	public password:string
	public fullName: string;
	public address : string;
	public phoneNumber: string
	public email: string;
	public gender:boolean;
	public birthDay:Date;
	public avatar: string;
	public expiredDate:Date;
	public enableStatus:boolean


	constructor(){}
}