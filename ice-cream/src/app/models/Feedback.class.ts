export class Feedback {
	public id:number;
	public fullName: string;
	public title:string;
	public content:string;
}