import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UserRecipe } from "./../../models/UserReipe.class";
import { YourRecipeService } from "./../../services/your-recipe.service";


@Component({
	selector: 'app-your-recipe',
	templateUrl: './your-recipe.component.html',
	styleUrls: ['./your-recipe.component.css']
})
export class YourRecipeComponent implements OnInit, OnDestroy {

	public yourRecipeForm: FormGroup;
	public userRecipe: UserRecipe;
	public file: File;
	public router: Router;
	public subscription: Subscription;
	public yourRecipeService: YourRecipeService;

	constructor(yourRecipeService: YourRecipeService, router: Router) {
		this.userRecipe = new UserRecipe();
		this.router = router;
		this.yourRecipeService = yourRecipeService;
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	ngOnInit() {

		this.yourRecipeForm = new FormGroup({
			'name': new FormControl(this.userRecipe.name, [
				Validators.required
			]),
			'desciption': new FormControl(this.userRecipe.desciption, [
				Validators.required
			]),
			'details': new FormControl(this.userRecipe.details, [
				Validators.required
			]),
			'image': new FormControl(this.userRecipe.image, [
				Validators.required
			])
		});
	}

	onResetForm() {
		this.yourRecipeForm.reset();
	}

	changeListener($event): void {
		this.file = $event.target.files[0];
	}

	onSubmitYourRecipe() {
		if (!this.yourRecipeForm.invalid) {
			this.userRecipe = this.yourRecipeForm.value;
			this.userRecipe.enablestatus = true;
			this.userRecipe.prizeStatus = true;

			this.userRecipe.image = "images/userrecipe/" + this.file.name;

			this.subscription = this.yourRecipeService.createUserRecipe(this.file, this.userRecipe).subscribe(res => {
				this.router.navigate(['']);
			}, error => {
				this.yourRecipeService.handleError(error);
			});
		}
	}

}
