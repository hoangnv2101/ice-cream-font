import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FeedbackService } from "./../../services/feedback.service";
import { Feedback } from "./../../models/Feedback.class";


@Component({
	selector: 'app-feedback',
	templateUrl: './feedback.component.html',
	styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit, OnDestroy {

	public subscription: Subscription;
	public feedbackForm: FormGroup;
	public feedback: Feedback;

	constructor(private feedbackService: FeedbackService, private router: Router) {
		this.feedback = new Feedback();
	}

	ngOnInit() {
		this.feedbackForm = new FormGroup({
			'fullName': new FormControl(this.feedback.fullName, [
				Validators.required,
				Validators.maxLength(50)
			]),
			'title': new FormControl(this.feedback.title, [
				Validators.required,
				Validators.maxLength(100)
			]),
			'content': new FormControl(this.feedback.content, [
				Validators.required

			])
		});
	}
	onSubmitFeedBack() {
		if (!this.feedbackForm.invalid) {
			this.feedback = this.feedbackForm.value;
			this.subscription = this.feedbackService.createFeedBack(this.feedback).subscribe(res => {
				this.router.navigate(['']);
			}, error => {
				this.feedbackService.handleError(error);
			});
		}
	}
	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

	}

	onResetForm() {
		this.feedbackForm.reset();
	}
}
