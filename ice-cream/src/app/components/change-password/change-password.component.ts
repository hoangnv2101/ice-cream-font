import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ChangePassword } from "./../../models/ChangePassword.class";
import { CustomerService } from "./../../services/customer.service";
import {TokenStorage} from "./../../utils/TokenStorage.class";
import { DataService } from './../../services/data.service';

function passwordConfirming(c: AbstractControl): any {
	if (!c.parent || !c) return;
	const pwd = c.parent.get('password');
	const cpwd = c.parent.get('confirm_password')

	if (!pwd || !cpwd) return;
	if (pwd.value !== cpwd.value) {
		return { invalid: true };

	}
}


@Component({
	selector: 'app-change-password',
	templateUrl: './change-password.component.html',
	styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
	public changePassword: ChangePassword;
	public subscription: Subscription;
	public changePassForm: FormGroup;

	constructor(private customerService: CustomerService, private router: Router, private tokenStorage:TokenStorage,private ds: DataService) {
		this.changePassword = new ChangePassword();
	}

	ngOnInit() {

		this.changePassForm = new FormGroup({
			'oldPass': new FormControl(this.changePassword.oldPass, [
				Validators.required
			]),
			'password': new FormControl(this.changePassword.password, [
				Validators.required,
				Validators.minLength(8),
				Validators.maxLength(20)
			]),
			'confirm_password': new FormControl(null, [
				Validators.required,
				passwordConfirming,
				Validators.minLength(8),
				Validators.maxLength(20)

			])
		});

	}

	onSubmitChangPassword(){
			if (!this.changePassForm.invalid) {
			this.changePassword = this.changePassForm.value;
			this.subscription = this.customerService.changePasword(this.changePassword).subscribe(res => {
			this.tokenStorage.signOut();
			this.ds.sendLogout(true);
			this.router.navigate(['']);
			}, error => {
				this.ds.sendLogout(true);
				this.customerService.handleError(error);
			});
		}
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

	}

}
