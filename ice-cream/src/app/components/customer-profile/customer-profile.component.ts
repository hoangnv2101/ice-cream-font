import { Component, OnInit, OnDestroy } from '@angular/core';
import {Location} from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { CustomerService } from './../../services/customer.service';
import { Customer } from './../../models/Customer.class';
import { TokenStorage } from "./../../utils/TokenStorage.class";
import { LoginUser } from "./../../models/LoginUser.class"
import * as moment from 'moment';


@Component({
	selector: 'app-customer-profile',
	templateUrl: './customer-profile.component.html',
	styleUrls: ['./customer-profile.component.css']
})
export class CustomerProfileComponent implements OnInit, OnDestroy {
	public registerForm: FormGroup;
	public customer: Customer;
	public router: Router;

	public subscription: Subscription;
	public customerService: CustomerService;
	public domSanitizer: DomSanitizer;
	public imagePath: SafeResourceUrl = null;
	public file: File;
	public tokenStorage: TokenStorage;
	public loginUser: LoginUser;
	public imgLink: string = null;
	public isUpdate : boolean = false;

	constructor(customerService: CustomerService,
	 router: Router, domSanitizer: DomSanitizer, tokenStorage: TokenStorage,private _location: Location) {
		this.customer = new Customer()
		this.customerService = customerService;
		this.router = router;
		this.domSanitizer = domSanitizer;
		this.tokenStorage = tokenStorage;
	}

	ngOnInit() {
		this.loginUser = this.tokenStorage.getLoginUser()

		if (this.loginUser && this.loginUser.username) {

			this.subscription = this.customerService.getCustomer(this.loginUser.id).subscribe(res => {
				this.customer = res;
				this.imgLink = this.customer.avatar;

				this.registerForm.setValue({
					fullName: this.customer.fullName,
					address: this.customer.address,
					phoneNumber: this.customer.phoneNumber,
					gender: this.customer.gender,
					avatar: "",
					email: this.customer.email,
					birthDay: moment(this.customer.birthDay).format("MM/DD/YYYY")
				});
			});


		}

		this.registerForm = new FormGroup({
			'fullName': new FormControl(this.customer.fullName, [
				Validators.required
			]),
			'address': new FormControl(this.customer.address, [
			]),
			'phoneNumber': new FormControl(this.customer.address, [
			]),
			'gender': new FormControl(this.customer.gender, [
			]),
			'avatar': new FormControl(this.customer.avatar, [
			]),
			'email': new FormControl(this.customer.email, [
				Validators.required,
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			]),
			'birthDay': new FormControl(this.customer.birthDay, [
				Validators.pattern("^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$")
			])
		});

		console.log(this.registerForm);
	}


	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}


	onSubmitCustomer() {
		if (!this.registerForm.invalid) {
			this.customer = this.registerForm.value;
			this.customer.birthDay = moment(this.customer.birthDay, "MM/DD/YYYY").toDate();
			this.customer.avatar = null;
			if (this.file) {
				this.customer.avatar = "images/uploaded/" + this.file.name;
			}

			this.subscription = this.customerService.updateCustomer(this.file, this.customer).subscribe(res => {
			this.router.navigate(['']);
			}, error => {
				this.customerService.handleError(error);
			});
		}
	}

	onBackForm() {
		  this._location.back();
	}

	changeListener($event): void {
		this.file = $event.target.files[0];
		this.readThis(this.file);
	}

	readThis(file: File): void {
		var myReader: FileReader = new FileReader();
		myReader.readAsDataURL(file);
		myReader.onloadend = (e) => {
			this.imagePath = this.domSanitizer.bypassSecurityTrustResourceUrl(myReader.result);
		}
	}

	onUpdateProfile()
	{
		this.isUpdate = true;
	}
}
