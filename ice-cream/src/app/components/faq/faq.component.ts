import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FAQ } from "./../../models/FAQ.class";
import {FaqService} from "./../../services/faq.service";


@Component({
	selector: 'app-faq',
	templateUrl: './faq.component.html',
	styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit, OnDestroy {

	public subscription: Subscription;
	public faqs: FAQ[];

	constructor(private faqService:FaqService) { }

	ngOnInit() {
		this.subscription = this.faqService.getAll().subscribe(res =>{
			this.faqs = res;
		});
	}
	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

	}
}
