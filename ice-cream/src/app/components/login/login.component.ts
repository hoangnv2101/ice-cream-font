import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { AuthenticateService } from "./../../services/authenticate.service";
import { Login } from "./../../models/Login.class";
import { DataService } from './../../services/data.service';
import { LoginUser } from "./../../models/LoginUser.class";
import { TokenStorage } from "./../../utils/TokenStorage.class";


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

	public loginForm: FormGroup;
	public login: Login;
	public router: Router;

	public activatedRoute: ActivatedRoute;
	public subscription: Subscription;
	public authenticateService: AuthenticateService;
	public isRemember: boolean = false;
	public username: string;
	public tokenStorage: TokenStorage;

	constructor(authenticateService: AuthenticateService, private location: Location, router: Router, private ds: DataService, tokenStorage: TokenStorage) {
		this.login = new Login();
		this.router = router;
		this.authenticateService = authenticateService;
		this.tokenStorage = tokenStorage;
	}

	ngOnInit() {
		this.loginForm = new FormGroup({
			'username': new FormControl(this.login.username, [
				Validators.required,
				Validators.minLength(4),
				Validators.maxLength(100)
			]),
			'password': new FormControl(this.login.password, [
				Validators.required
			])
		});
	}

	onSubmit() {
		if (!this.loginForm.invalid) {
			this.login = this.loginForm.value;

			this.subscription = this.authenticateService.loginUser(this.login).subscribe(res => {
				console.log(this.tokenStorage.parseToken(res.token));
				let usr : LoginUser= this.tokenStorage.parseToken(res.token);
				if (usr.role === "ROLE_USER") {
					
					this.tokenStorage.saveToken(res, this.isRemember);
					let loginUser = this.tokenStorage.getLoginUser();
					this.username = loginUser.username;
					this.ds.sendData(this.username);
					this.ds.sendLogout(false);
					this.router.navigate(['']);
					window.location.reload();
				}
			}, error => {
				this.authenticateService.handleError(error);
			});
		}
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

}
