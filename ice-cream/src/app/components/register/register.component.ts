import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { CustomerService } from './../../services/customer.service';
import { Customer } from './../../models/Customer.class';
import { FileService } from './../../services/file.service';
import * as moment from 'moment';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {

	public registerForm: FormGroup;
	public customer: Customer;
	public router: Router;

	public activatedRoute: ActivatedRoute;
	public subscription: Subscription;
	public customerService: CustomerService;
	public domSanitizer: DomSanitizer;
	public imagePath: SafeResourceUrl = null;
	public fileService: FileService;
	public file: File;


	constructor(activatedRoute: ActivatedRoute, customerService: CustomerService, router: Router, domSanitizer: DomSanitizer, fileService: FileService) {
		this.customer = new Customer();
		this.customerService = customerService;
		this.activatedRoute = activatedRoute
		this.router = router;
		this.domSanitizer = domSanitizer;
		this.fileService = fileService;
	}

	ngOnInit() {
		this.registerForm = new FormGroup({
			'username': new FormControl(this.customer.username, [
				Validators.required,
				Validators.minLength(4),
				Validators.maxLength(100)
			]),
			'password': new FormControl(this.customer.password, [
				Validators.required,
				Validators.minLength(8),
				Validators.maxLength(20),
			]),
			'fullName': new FormControl(this.customer.fullName, [
				Validators.required
			]),
			'address': new FormControl(this.customer.address, [
			]),
			'phoneNumber': new FormControl(this.customer.address, [
			]),
			'gender': new FormControl(this.customer.gender, [
			]),
			'avatar': new FormControl(this.customer.avatar, [
			]),
			'email': new FormControl(this.customer.email, [
				Validators.required,
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			]),
			'birthDay': new FormControl(this.customer.birthDay, [
				Validators.pattern("^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$")
			])
		});

		//load default avatar
		this.fileService.getDefaultImage().subscribe(res => {
			let file = new File([res], 'noavatar.gif', { type: 'image/gif' });
			this.readThis(file);
		}, error => {
		});
	}


	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}


	onSubmitCustomer() {
		if (!this.registerForm.invalid) {
			this.customer = this.registerForm.value;
			this.customer.birthDay = moment(this.customer.birthDay, "MM/DD/YYYY").toDate();

			if (this.file) {
				this.customer.avatar = "images/uploaded/" + this.file.name;
			}
			else {
				this.customer.avatar = "images/uploaded/noavatar.gif";
			}

			this.subscription = this.customerService.createCustomer(this.file, this.customer).subscribe(res => {
				this.router.navigate(['']);
			}, error => {
				this.customerService.handleError(error);
			});
		}
	}

	onResetForm() {
		this.registerForm.reset();
	}

	changeListener($event): void {
		this.file = $event.target.files[0];
		this.readThis(this.file);
	}

	readThis(file: File): void {
		var myReader: FileReader = new FileReader();
		myReader.readAsDataURL(file);
		myReader.onloadend = (e) => {
			this.imagePath = this.domSanitizer.bypassSecurityTrustResourceUrl(myReader.result);
		}
	}
}
