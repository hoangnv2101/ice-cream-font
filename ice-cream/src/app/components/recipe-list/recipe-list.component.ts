import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ricipe } from './../../models/Recipe.class';
import { Subscription } from 'rxjs';
import { RecipeService } from './../../services/recipe.service';


@Component({
	selector: 'app-recipe-list',
	templateUrl: './recipe-list.component.html',
	styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {

	public subscription: Subscription;
	public recipeService: RecipeService;
	public ricipes: Ricipe[];


	constructor(recipeService: RecipeService) {
		this.recipeService = recipeService;
	}

	ngOnInit() {
		this.loadData();
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	loadData() {
		this.subscription =
			this.recipeService.getTAllRecipes().subscribe(res => {
				this.ricipes = res;
			}, error => {
				this.recipeService.handleError(error);
			});
	}

}
