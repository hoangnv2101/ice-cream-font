import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { RecipeService } from './../../services/recipe.service';
import { Ricipe } from './../../models/Recipe.class';


@Component({
	selector: 'app-multiple-carousel',
	templateUrl: './multiple-carousel.component.html',
	styleUrls: ['./multiple-carousel.component.css']
})
export class MultipleCarouselComponent implements OnInit {

	public subscription: Subscription;
	public recipeService: RecipeService;
	public ricipes: Ricipe[];
	public map = new Map<number, Ricipe[]>();

	constructor(recipeService: RecipeService) {
		this.recipeService = recipeService;
	}
	ngOnInit() {
		this.getAllRecipes();
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	getAllRecipes() {
		this.subscription =
			this.recipeService.getTAllRecipes().subscribe(res => {
				let i = 0
				let count = 0;

				this.ricipes = res;

				for (i; i < this.ricipes.length / 4; i++) {
					this.map.set(i, this.ricipes.slice(count, count + 4));
					count += 4;
				}
				if (this.ricipes.length % 4 != 0) {
					this.map.set(i, this.ricipes.slice(count, this.ricipes.length));
				}
			}, error => {
				this.recipeService.handleError(error);
			});
	}

	getKeys() {
		return Array.from(this.map.keys());
	}

	getValue(key: number) {
		return Array.from(this.map.get(key));
	}
}
