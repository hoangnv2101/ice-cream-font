import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Ricipe } from './../../models/Recipe.class';
import { Subscription } from 'rxjs';
import { RecipeService } from './../../services/recipe.service';

@Component({
	selector: 'app-recipe',
	templateUrl: './recipe.component.html',
	styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit, OnDestroy {

	public activatedRoute: ActivatedRoute;
	public subscription: Subscription;
	public recipeService: RecipeService;
	public ricipes: Ricipe[];
	public ricipe: Ricipe;

	constructor(activatedRoute: ActivatedRoute, recipeService: RecipeService) {
		this.recipeService = recipeService;
		this.activatedRoute = activatedRoute
	}

	ngOnInit() {
		this.loadData();

	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	loadData() {
		this.subscription = this.activatedRoute.params.subscribe(data => {
			let id = +data.id;
			this.recipeService.updateViewNumber(id).subscribe(res => {				
				this.ricipe = res;
				 window.scroll(0,0);
			}, error => {
				this.recipeService.handleError(error);
			});
		});

	}
}


