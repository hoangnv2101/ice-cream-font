import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { RecipeService } from './../../services/recipe.service';
import { Ricipe } from './../../models/Recipe.class';
import { TokenStorage } from "./../../utils/TokenStorage.class";
import { DataService } from './../../services/data.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

	public subscription: Subscription;
	public recipeService: RecipeService;
	public ricipes: Ricipe[];
	public newestRecipes: Ricipe[];
	public isLogin: boolean = false;

	constructor(recipeService: RecipeService, private tokenStorage: TokenStorage, private ds: DataService) {
		this.recipeService = recipeService;
	}

	ngOnInit() {
		this.getTop10View();
		this.getTop10Newest();

		this.subscription = this.ds.getData().subscribe(x => {
			if (typeof x === "boolean") {
				this.isLogin = false;
			}
		});

		if (this.tokenStorage.getLoginUser() && this.tokenStorage.getLoginUser().username) {
			this.isLogin = true;
		}
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	getTop10View() {
		this.subscription = this.recipeService.getTopTenViewNumber().subscribe(res => {
			this.ricipes = res;
		}, error => {
			this.recipeService.handleError(error);
		});;
	}

	getTop10Newest() {
		this.subscription = this.recipeService.getTopTenNewest().subscribe(res => {
			this.newestRecipes = res;
		}, error => {
			this.recipeService.handleError(error);
		});;
	}


}
