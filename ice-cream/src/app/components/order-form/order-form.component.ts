import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Ricipe } from './../../models/Recipe.class';
import { Subscription } from 'rxjs';

import { OrderBook } from './../../models/OrderBook.class';
import { OrderService } from './../../services/order.service';

@Component({
	selector: 'app-order-form',
	templateUrl: './order-form.component.html',
	styleUrls: ['./order-form.component.css']
})
export class OrderFormComponent implements OnInit, OnDestroy {

	public orderForm: FormGroup;
	public orderBook: OrderBook;
	public router: Router;

	public activatedRoute: ActivatedRoute;
	public subscription: Subscription;
	public orderService: OrderService;


	constructor(activatedRoute: ActivatedRoute, orderService: OrderService, router: Router) {
		this.orderBook = new OrderBook();
		this.orderService = orderService;
		this.activatedRoute = activatedRoute
		this.router = router;
	}


	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	ngOnInit() {
		this.orderForm = new FormGroup({
			'name': new FormControl(this.orderBook.name, [
				Validators.required,
				Validators.minLength(4),
				Validators.maxLength(50)
			]),
			'email': new FormControl(this.orderBook.email, [
				Validators.required,
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			]),
			'contact': new FormControl(this.orderBook.contact, [
				Validators.required
			]),
			'address': new FormControl(this.orderBook.address, [
				Validators.required
			]),
			'cardNumber': new FormControl(this.orderBook.cardNumber, [
				Validators.required,
				Validators.pattern('^[0-9]{16}')
			]),
			'cardName': new FormControl(this.orderBook.cardName, [
				Validators.required
			]),
			'expiration': new FormControl(this.orderBook.expiration, [
				Validators.required,
				Validators.pattern('(0[1-9]|10|11|12)/[1-9]{2}$')
			]),
			'cvv': new FormControl(this.orderBook.expiration, [
				Validators.required,
				Validators.pattern('^[0-9]{3}')
			]),
			'payingOption': new FormControl(this.orderBook.payingOption, [
				Validators.required

			])

		});
	}

	onSubmitOrder() {
		if (!this.orderForm.invalid) {
			this.orderBook = this.orderForm.value;
			this.orderBook.orderDate = new Date();
			this.orderBook.bookCost = 49.99;
			this.orderBook.status = true;
			this.subscription = this.orderService.submitOrder(this.orderBook).subscribe(res => {
			this.router.navigate(['']);
			}, error => {
				this.orderService.handleError(error);
			});
		}
	}

	onResetForm()
	{
		this.orderForm.reset();
	}
}

