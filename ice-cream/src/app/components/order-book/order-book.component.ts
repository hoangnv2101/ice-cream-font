import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-order-book',
	templateUrl: './order-book.component.html',
	styleUrls: ['./order-book.component.css']
})
export class OrderBookComponent implements OnInit {

	public isShow: boolean = false;
	
	constructor() { }

	ngOnInit() {
	}

	clickToOrder(el) {
		console.log(el);
		this.isShow = true;
		 el.scrollIntoView(true);
	}

}
