import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AdminModule } from "./admin/admin.module";


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JWtInterceptorService } from './services/jwt-interceptor.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';

import { RecipeService } from './services/recipe.service';
import { FormatDataPipe } from './pipe/format-data.pipe';
import { RecipeComponent } from './components/recipe/recipe.component';
import { OrderBookComponent } from './components/order-book/order-book.component';
import { RecipeListComponent } from './components/recipe-list/recipe-list.component';
import { LoginComponent } from './components/login/login.component';
import { FooterComponent } from './components/footer/footer.component';
import { MultipleCarouselComponent } from './components/multiple-carousel/multiple-carousel.component';
import { OrderFormComponent } from './components/order-form/order-form.component';
import { RegisterComponent } from './components/register/register.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { CustomerProfileComponent } from './components/customer-profile/customer-profile.component';
import { YourRecipeComponent } from './components/your-recipe/your-recipe.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { FaqComponent } from './components/faq/faq.component';
import { AuthGuard } from "./services/guards/auth.guard";
import {SharedModule} from "./shared/shared.module";


const appRoutes: Routes = [
	{
		path: '',
		component: HomeComponent

	},
	{
		path: 'oderBook',
		component: OrderBookComponent
	},
	{
		path: 'recipe/:id',
		component: RecipeComponent
	},
	{
		path: 'feedback',
		component: FeedbackComponent
	},
	{
		path: 'register',
		component: RegisterComponent
	},
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'profile',
		canActivate: [AuthGuard],
		component: CustomerProfileComponent
	},
	{
		path: 'your-recipe',
		canActivate: [AuthGuard],
		component: YourRecipeComponent
	},
	{
		path: 'changePass',
		canActivate: [AuthGuard],
		component: ChangePasswordComponent
	},
	{
		path: 'faq',
		component: FaqComponent
	}
];

@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
		HomeComponent,
		FormatDataPipe,
		RecipeComponent,
		OrderBookComponent,
		RecipeListComponent,
		LoginComponent,
		FooterComponent,
		MultipleCarouselComponent,
		OrderFormComponent,
		RegisterComponent,
		FeedbackComponent,
		CustomerProfileComponent,
		YourRecipeComponent,
		ChangePasswordComponent,
		FaqComponent
	],
	imports: [
		BrowserModule,
		ReactiveFormsModule,
		FormsModule,
		AdminModule,
		SharedModule,
		RouterModule.forRoot(appRoutes),
		HttpClientModule
	],
	providers: [{
		provide: HTTP_INTERCEPTORS,
		useClass: JWtInterceptorService,
		multi: true
	}
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
