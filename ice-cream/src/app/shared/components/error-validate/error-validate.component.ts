import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-error-validate',
	templateUrl: './error-validate.component.html',
	styleUrls: ['./error-validate.component.css']
})
export class ErrorValidateComponent implements OnInit {

	@Input('control') control;
	@Input('controlName') controlName;

	constructor() { }

	ngOnInit() {
	}

	get message() {
		for (let err in this.control.errors) {
			if (this.control.invalid && (this.control.dirty || this.control.touched)) {
				return this.getErrorMessage(err, this.control.errors[err]);
			}
		}
	}

	getErrorMessage(err, value) {
		let messages = {
			'required': `${this.controlName} is required.`,
			'minlength': `${this.controlName} must be at least ${value.requiredLength} characters long`,
			'maxlength': `${this.controlName} must be maximum ${value.requiredLength} characters long`,
			'pattern': `${this.controlName} invalid`,
			'invalid': `The confirm password doest not match new pasword`

		}
		return messages[err];
	}

}
