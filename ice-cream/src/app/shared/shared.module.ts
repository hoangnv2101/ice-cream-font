import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorValidateComponent } from "./components/error-validate/error-validate.component";
import { FormatDataListPipe } from './pipe/format-data-list.pipe';


@NgModule({
	imports: [
		CommonModule
	],
	declarations: [FormatDataListPipe, ErrorValidateComponent],
	exports: [
		ErrorValidateComponent,FormatDataListPipe
	]
})
export class SharedModule { }
