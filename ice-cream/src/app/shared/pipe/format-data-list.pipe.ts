import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatDataList'
})
export class FormatDataListPipe implements PipeTransform {

  transform(value: any, args?: any): any {
  return value.replace(/(?:\\n\\r)/g, '<br />');
  }

}
