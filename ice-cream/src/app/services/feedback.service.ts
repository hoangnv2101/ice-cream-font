import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Feedback } from "./../models/Feedback.class"

@Injectable({
	providedIn: 'root'
})
export class FeedbackService {
	public API: string = 'http://localhost:8080/rest/feedback';
	public httpClient: HttpClient;

	constructor(httpClient: HttpClient) {
		this.httpClient = httpClient;
	}

	createFeedBack(feedBack: Feedback): Observable<Feedback> {
		return this.httpClient.post<Feedback>(this.API, feedBack);
	}

	getAll() : Observable<Feedback[]> {
		return this.httpClient.get<Feedback[]>(this.API);
	}

	delete(id:number):Observable<any> {
		return this.httpClient.delete(`${this.API}/${id}`);
	}

	handleError(err: any) {
		if (err.error instanceof Error) {
			console.log('client site error : ' + err.message);
		}
		else {
			console.log('client site erro ' + err.status + ' - ' + err.message);
		}
	}
}
