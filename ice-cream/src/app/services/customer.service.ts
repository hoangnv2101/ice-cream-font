import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from './../models/Customer.class';
import { ChangePassword } from "./../models/ChangePassword.class";


@Injectable({
	providedIn: 'root'
})
export class CustomerService {

	public API: string = 'http://localhost:8080/rest/customers';
	public httpClient: HttpClient;

	constructor(httpClient: HttpClient) {
		this.httpClient = httpClient;
	}

	createCustomer(file: File, customer: Customer): Observable<any> {

		let formdata: FormData = new FormData();
		if (file) {
			formdata.append('file', file);
		}
		formdata.append('customer', new Blob([JSON.stringify(customer)], { type: 'application/json' }));

		return this.httpClient.post(this.API, formdata);
	}

	updateCustomer(file: File, customer: Customer): Observable<any> {

		let formdata: FormData = new FormData();
		if (file) {
			formdata.append('file', file);
		}
		formdata.append('customer', new Blob([JSON.stringify(customer)], { type: 'application/json' }));

		return this.httpClient.patch(this.API, formdata);
	}

	updateCustomerById(id:number, customer: Customer): Observable<any> {

		console.log(customer.expiredDate);
		return this.httpClient.patch(`${this.API}/${id}`, customer);
	}


	updateListCustomers(customers: Customer[]): Observable<any> {

		return this.httpClient.patch(`${this.API}/update`, customers);
	}

	getCustomer(id: number): Observable<Customer> {
		return this.httpClient.get<Customer>(`${this.API}/${id}`);
	}

	getCustomers(): Observable<Customer[]> {
		return this.httpClient.get<Customer[]>(`${this.API}`);
	}

	changePasword(ChangePassword: ChangePassword): Observable<any> {
		return this.httpClient.post(`${this.API}/changePassword`, ChangePassword);
	}

	handleError(err: any) {
		if (err.error instanceof Error) {
			console.log('client site error : ' + err.message);
		}
		else {
			console.log('client site erro ' + err.status + ' - ' + err.message);
		}
	}
}
