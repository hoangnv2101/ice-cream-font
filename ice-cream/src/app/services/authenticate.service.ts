import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Login} from './../models/Login.class';

@Injectable({
	providedIn: 'root'
})
export class AuthenticateService {
	public API: string = 'http://localhost:8080/rest/token/generate-token';
	public httpClient: HttpClient;

	constructor(httpClient: HttpClient) {
		this.httpClient = httpClient;
	}

	loginUser(login:Login):Observable<any> {
		return this.httpClient.post<any>(this.API, login);
	}

	handleError(err: any) {
		if (err.error instanceof Error) {
			console.log('client site error : ' + err.message);
		}
		else {
			console.log('client site erro ' + err.status + ' - ' + err.message);
		}
	}
}
