import { TestBed, inject } from '@angular/core/testing';

import { JWtInterceptorService } from './jwt-interceptor.service';

describe('JWtInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JWtInterceptorService]
    });
  });

  it('should be created', inject([JWtInterceptorService], (service: JWtInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
