import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OrderBook } from './../models/OrderBook.class';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class OrderService {

	public API: string = 'http://localhost:8080/rest/orders';

	public httpClient: HttpClient;

	constructor(httpClient: HttpClient) {
		this.httpClient = httpClient;
	}

	submitOrder(orderBook: OrderBook): Observable<OrderBook> {

		return this.httpClient.post<OrderBook>(this.API, orderBook);
	}

	getAll(): Observable<OrderBook[]> {

		return this.httpClient.get<OrderBook[]>(this.API);
	}

	updateStatus(id: number, orderBook:OrderBook): Observable<OrderBook> {
		return this.httpClient.patch<OrderBook>(`${this.API}/${id}`, orderBook)
	}

	getOrder(id: number): Observable<OrderBook> {

		return this.httpClient.get<OrderBook>(`${this.API}/${id}`)
	}

	handleError(err: any) {
		if (err.error instanceof Error) {
			console.log('client site error : ' + err.message);
		}
		else {
			console.log('client site erro ' + err.status + ' - ' + err.message);
		}
	}

}
