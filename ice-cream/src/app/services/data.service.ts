import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';


@Injectable({
	providedIn: 'root'
})
export class DataService {
	private subject = new Subject<any>();

	constructor() { }

	sendData(message: string) {
		this.subject.next(message);
	}

	sendLogout(isLogout: boolean) {
		this.subject.next(isLogout);
	}

	clearData() {
		this.subject.next();
	}

	getData(): Observable<any> {
		return this.subject.asObservable();
	}
}
