import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FAQ } from "./../models/FAQ.class"

@Injectable({
	providedIn: 'root'
})
export class FaqService {

	public API: string = 'http://localhost:8080/rest/faq';
	public httpClient: HttpClient;

	constructor(httpClient: HttpClient) {
		this.httpClient = httpClient;
	}
	delete(id: number): Observable<any> {
		return this.httpClient.delete(`${this.API}/${id}`);
	}

	getAll(): Observable<FAQ[]> {
		return this.httpClient.get<FAQ[]>(`${this.API}`);
	}

	createFaq(faq: FAQ): Observable<FAQ> {
		return this.httpClient.post<FAQ>(this.API, faq);
	}

	
	handleError(err: any) {
		if (err.error instanceof Error) {
			console.log('client site error : ' + err.message);
		}
		else {
			console.log('client site erro ' + err.status + ' - ' + err.message);
		}
	}
}
