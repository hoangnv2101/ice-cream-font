import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserRecipe } from './../models/UserReipe.class';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class YourRecipeService {

	public API: string = 'http://localhost:8080/rest/user-recipe';
	public httpClient: HttpClient;


	constructor(httpClient: HttpClient) {
		this.httpClient = httpClient;
	}


	createUserRecipe(file: File, userRecipe: UserRecipe): Observable<any> {

		let formdata: FormData = new FormData();
		if (file) {
			formdata.append('file', file);
		}
		formdata.append('customer', new Blob([JSON.stringify(userRecipe)], { type: 'application/json' }));

		return this.httpClient.post(this.API, formdata);
	}

	getAllUserRecipes(): Observable<UserRecipe[]> {
		return this.httpClient.get<UserRecipe[]>(this.API);
	}

	getRecipe(id: number): Observable<UserRecipe> {

		return this.httpClient.get<UserRecipe>(`${this.API}/${id}`)
	}

	delete(id: number): Observable<any> {
		return this.httpClient.delete(`${this.API}/${id}`)
	}

	updateStatus(id: number, userRecipe:UserRecipe): Observable<UserRecipe> {
		return this.httpClient.patch<UserRecipe>(`${this.API}/${id}`, userRecipe)
	}


	handleError(err: any) {
		if (err.error instanceof Error) {
			console.log('client site error : ' + err.message);
		}
		else {
			console.log('client site erro ' + err.status + ' - ' + err.message);
		}
	}

}
