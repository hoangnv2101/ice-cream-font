import { Injectable } from '@angular/core';
import { finalize, tap } from 'rxjs/operators';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {TokenStorage} from "./../utils/TokenStorage.class";


@Injectable({
  providedIn: 'root'
})
export class JWtInterceptorService implements HttpInterceptor {

  public router: Router;

  constructor(router: Router, private tokenStorage:TokenStorage) {
    this.router = router;
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    let ok: string;

    let currentUser = JSON.parse(localStorage.getItem('currentUser') == null ? sessionStorage.getItem('currentUser') : localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.token}`
        }
      });
    }

    return next.handle(request).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          // do nothing
        }
      },
        // Operation failed; error is an HttpErrorResponse
        error => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 401 || error.status === 405 || error.status === 403) {

              this.tokenStorage.signOut();
              this.router.navigate(['login']);
            }
          }
        }
      ))
  }
}
