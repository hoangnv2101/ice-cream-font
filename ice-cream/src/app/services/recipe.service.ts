import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ricipe } from './../models/Recipe.class';
import { Observable } from 'rxjs';


@Injectable({
	providedIn: 'root'
})
export class RecipeService {

	public API: string = 'http://localhost:8080/rest/recipes';
	public sub_top_number = '/view/?maxResult=10'
	public sub_top_new = '/new/?maxResult=10'
	public httpClient: HttpClient;


	constructor(httpClient: HttpClient) {
		this.httpClient = httpClient;
	}

	getTopTenViewNumber(): Observable<Ricipe[]> {
		console.log(this.API + this.sub_top_number);

		return this.httpClient.get<Ricipe[]>(this.API + this.sub_top_number);
	}

	getTopTenNewest(): Observable<Ricipe[]> {

		return this.httpClient.get<Ricipe[]>(this.API + this.sub_top_new);
	}

	getTAllRecipes(): Observable<Ricipe[]> {

		return this.httpClient.get<Ricipe[]>(this.API);
	}

	getRecipeById(id: number): Observable<Ricipe> {

		return this.httpClient.get<Ricipe>(`${this.API}/${id}`);
	}

	updateViewNumber(id: number): Observable<Ricipe> {

		return this.httpClient.put<Ricipe>(`${this.API}/${id}/view`, null);
	}


	updateRicipe(file: File, ricipe: Ricipe): Observable<Ricipe> {

		let formdata: FormData = new FormData();
		if (file) {
			formdata.append('file', file);
		}
		formdata.append('recipe', new Blob([JSON.stringify(ricipe)], { type: 'application/json' }));

		return this.httpClient.patch<Ricipe>(this.API, formdata);
	}


	createRecipe(file: File, ricipe: Ricipe): Observable<Ricipe> {

		let formdata: FormData = new FormData();
		if (file) {
			formdata.append('file', file);
		}
		formdata.append('recipe', new Blob([JSON.stringify(ricipe)], { type: 'application/json' }));

		return this.httpClient.post<Ricipe>(this.API, formdata);
	}

	handleError(err: any) {
		if (err.error instanceof Error) {
			console.log('client site error : ' + err.message);
		}
		else {
			console.log('client site erro ' + err.status + ' - ' + err.message);
		}
	}
}
