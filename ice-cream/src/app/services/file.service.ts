import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class FileService {

	public httpClient: HttpClient;
	private API:string = "http://localhost:8080/rest"

	constructor(httpClient: HttpClient) {
		this.httpClient = httpClient;
	}

	public getDefaultImage(): Observable<Blob> {
		return this.httpClient
			.get(`./assets/images/noavatar.gif`, {
				responseType: "blob"
			});
	}

	public getUploadedImage(imageName:string): Observable<any> {
		return this.httpClient
			.get(`${this.API}/${imageName}`);
	}

	handleError(err: any) {
		if (err.error instanceof Error) {
			console.log('client site error : ' + err.message);
		}
		else {
			console.log('client site erro ' + err.status + ' - ' + err.message);
		}
	}
}
