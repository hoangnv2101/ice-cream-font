import { TestBed, inject } from '@angular/core/testing';

import { YourRecipeService } from './your-recipe.service';

describe('YourRecipeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YourRecipeService]
    });
  });

  it('should be created', inject([YourRecipeService], (service: YourRecipeService) => {
    expect(service).toBeTruthy();
  }));
});
