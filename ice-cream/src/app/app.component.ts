import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from './services/data.service';
import { Router } from '@angular/router';
import { TokenStorage } from "./utils/TokenStorage.class";


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy, OnInit {
	title = 'app';
	username: any = null;
	subscription: Subscription;
	isAdmin: boolean = false;

	constructor(private ds: DataService, private router: Router, private tokenStorage: TokenStorage) {

	}

	ngOnInit() {
		if (this.tokenStorage.getLoginUser()) {
			if (this.tokenStorage.getLoginUser().role === "ROLE_ADMIN") {
				this.isAdmin = true;

			} else {
				this.username = this.tokenStorage.getLoginUser().username;

				// subscribe to home component messages
				this.subscription = this.ds.getData().subscribe(x => {
					if (typeof x === "string") {
						this.username = x;
					}

				});
			}
		}
	}


	ngOnDestroy() {
		// unsubscribe to ensure no memory leaks
		this.subscription.unsubscribe();
	}

	logout() {
		this.tokenStorage.signOut();
		this.username = null;
		this.ds.sendLogout(true);
		this.router.navigate(['']);

	}

}
