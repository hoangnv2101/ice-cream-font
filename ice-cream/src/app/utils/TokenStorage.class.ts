import { Injectable } from '@angular/core';
import * as JWT from 'jwt-decode';
import { LoginUser } from "./../models/LoginUser.class";


@Injectable({
  providedIn: 'root'
})
export class TokenStorage {

  public loginUser: LoginUser = new LoginUser();
  constructor() { }

  signOut() {
    // remove user from local storage to log user out
    sessionStorage.removeItem('currentUser');
    localStorage.removeItem('currentUser');
  }

  public saveToken(token: string, isRemember: boolean) {
    if (isRemember) {
      localStorage.setItem('currentUser', JSON.stringify(token));
    }
    else {
      sessionStorage.setItem('currentUser', JSON.stringify(token));
    }
  }

  public getLoginUser(): LoginUser {
    let currentUser = JSON.parse(localStorage.getItem('currentUser') == null ? sessionStorage.getItem('currentUser') : localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      this.loginUser.username = JWT(currentUser.token)['sub'];
      this.loginUser.role = JWT(currentUser.token)['scopes'].authority;
      this.loginUser.id = JWT(currentUser.token)['id'];
      return this.loginUser;
    }
    return null
  }

  public parseToken(token: string): LoginUser {
    this.loginUser.username = JWT(token)['sub'];
    this.loginUser.role = JWT(token)['scopes'].authority;
    return this.loginUser;
  }


}
